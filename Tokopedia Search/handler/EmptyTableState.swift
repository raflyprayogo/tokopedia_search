//
//  EmptyTableState.swift
//  Tokopedia Search
//
//  Created by Rafly Prayogo on 16/07/18.
//  Copyright © 2018 Tokopedia. All rights reserved.
//

import UIKit

class EmptyTableState: UIView {
    @IBOutlet weak var imageIv: UIImageView!
    @IBOutlet weak var descLbl: UILabel!
    
    func setupWithSuperView(superView: UIView) {
        self.frame.size.width = 100
        self.frame.size.height = 100
        superView.addSubview(self)
        
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
    }
}
