//
//  ViewUtil.swift
//  Tokopedia Search
//
//  Created by Rafly Prayogo on 16/07/18.
//  Copyright © 2018 Tokopedia. All rights reserved.
//

import Foundation
import SwiftMessages

class ViewUtil {
    
    func failedMsg(msg: String){
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(.error)
        view.button?.isHidden = true
        view.configureContent(title: "Gagal", body: msg)
        SwiftMessages.show(view: view)
    }
    
    func successMsg(msg: String){
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(.success)
        view.button?.isHidden = true
        view.configureContent(title: "Sukses", body: msg)
        SwiftMessages.show(view: view)
    }
    
    func infoMsg(msg: String){
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(.info)
        view.button?.isHidden = true
        view.titleLabel?.isHidden = true
        view.configureContent(body: msg)
        SwiftMessages.show(view: view)
    }
    
    func checkConn(){
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureTheme(.error)
        view.configureContent(body: "Tidak dapat terhubung ke server")
        SwiftMessages.show(view: view)
    }
    
    func noConn(){
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureTheme(.error)
        view.configureContent(body: "Tidak dapat terhubung")
        SwiftMessages.show(view: view)
    }
    
    func validateTFEmpty(_ tFields : [UITextField]) -> Bool {
        var i : Int = 0
        for tfield in tFields{
            if(tfield.text! == ""){
                ViewUtil().failedMsg(msg: tfield.placeholder!+" tidak boleh kosong")
                return false
            }
            i += 1
        }
        return true
    }
    
    func validateStringEmpty(_ str : String, _ strName:String) -> Bool {
        if(str == ""){
            ViewUtil().failedMsg(msg: strName+" tidak valid")
            return false
        }
        return true
    }
    
    func validateTFNumeric(_ tFields : [UITextField]) -> Bool {
        var i : Int = 0
        for tfield in tFields{
            if !tfield.text!.isNumber{
                ViewUtil().failedMsg(msg: tfield.placeholder!+" hanya nomor")
                return false
            }
            i += 1
        }
        return true
    }
    
    func setEmptyStateCollectionView(_ collectionView: UICollectionView, _ dataSetCount: Int, _ title: String, _ icon: UIImage ){
        let emptyState = Bundle.main.loadNibNamed("EmptyTableState", owner: nil, options: nil)?.first as! EmptyTableState
        DispatchQueue.main.async {
            emptyState.imageIv.image     = icon
            emptyState.descLbl.text    = title
        }
        if (dataSetCount == 0){
            collectionView.backgroundView = emptyState
        }else{
            collectionView.backgroundView = nil
        }
    }
}

