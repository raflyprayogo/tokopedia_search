//
//  SearchViewController.swift
//  Tokopedia Search
//
//  Created by Rafly Prayogo on 16/07/18.
//  Copyright © 2018 Tokopedia. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import UIScrollView_InfiniteScroll

class SearchGridCell: UICollectionViewCell{
    @IBOutlet weak var thumbnailIv: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
}

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchResultsUpdating, UISearchBarDelegate, FilterDelegate  {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var tblvContent     = [[String]]()
    var refresher       = UIRefreshControl()
    var progressView:ProgressView?
    
    var isWholeSale     = false
    var minPrice        = 0
    var maxPrice        = 1000000
    var page:Int        = 0
    
    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 2,
        minimumInteritemSpacing: 0,
        minimumLineSpacing: 0,
        sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    )

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate                 = self
        collectionView.dataSource               = self
        collectionView.collectionViewLayout     = columnLayout
        collectionView.contentInsetAdjustmentBehavior = .always
        collectionView.addInfiniteScroll { (collectionView) in
            self.page = self.page + 10
            self.getData(true)
            print("tadaaaaaa")
        }
        
        refresher.tintColor = UIColor.gray
        refresher.addTarget(self, action: #selector(self.getData(_:)), for: .valueChanged)
        collectionView!.addSubview(refresher)
        refresher.beginRefreshing()
        
        searchBar.delegate = self
        searchBar.sizeToFit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.collectionView.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.progressView = ProgressView()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        ViewUtil().setEmptyStateCollectionView(self.collectionView, tblvContent.count, "Your search product appear here", UIImage(named: "tokopedia")!)
        return tblvContent.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchGridCell", for: indexPath) as! SearchGridCell
        
        cell.nameLbl.text        = tblvContent[indexPath.row][0]
        cell.priceLbl.text       = tblvContent[indexPath.row][1]
        
        cell.thumbnailIv.af_setImage(withURL: URL(string: tblvContent[indexPath.row][2])!, placeholderImage: UIImage(named: "no_image"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let collectionViewWidth = collectionView.bounds.width/3.0
        let collectionViewHeight = collectionViewWidth
        
        return CGSize(width: collectionViewWidth, height: collectionViewHeight)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if(!(searchBar.text?.isEmpty)!){
            getData(false)
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
    }
    
    @objc func getData(_ fromInfinite:Bool){
        if(!fromInfinite){
            page = 0
            self.progressView?.show()
        }
        let parameters: Parameters = [
            "q": searchBar.text!,
            "pmin": String(minPrice),
            "pmax": String(maxPrice),
            "wholesale":isWholeSale,
            "official":true,
            "fshop":"2",
            "start": String(page),
            "rows": String(page+10)
        ]
        Alamofire.request("https://ace.tokopedia.com/search/v2.5/product" , method: .get, parameters: parameters).responseJSON { response in
            switch response.result {
            case .success:
                self.progressView?.hide()
                if(!fromInfinite){
                    self.tblvContent.removeAll()
                }
                let json    = JSON(response.result.value!)
                print(json)
                let results = json["data"].arrayObject!
                for resItem in results as! [Dictionary<String, AnyObject>] {
                    let name    = resItem["name"] as? String ?? ""
                    let price   = resItem["price"] as? String ?? ""
                    let image   = resItem["image_uri"] as? String ?? ""
                    self.tblvContent.append([name,price,image])
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    if(!fromInfinite){
                        if(self.tblvContent.count > 0){
                            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                        }
                    }
                    if(self.collectionView.isAnimatingInfiniteScroll){
                        self.collectionView.finishInfiniteScroll()
                    }
                    if(self.refresher.isRefreshing){
                        self.refresher.endRefreshing()
                    }
                }
            case .failure(let error):
                print(error)
                self.progressView?.hide()
                if(self.refresher.isRefreshing){
                    self.refresher.endRefreshing()
                }
                ViewUtil().checkConn()
            }
        }
    }

    @IBAction func btnFilter(_ sender: Any) {
        let destVC : FilterViewController = self.storyboard!.instantiateViewController(withIdentifier: "FilterVC") as! FilterViewController
        destVC.title                    = "Filter"
        destVC.delegate                 = self
        destVC.minPrice                 = minPrice
        destVC.maxPrice                 = maxPrice
        destVC.isWholeSale              = isWholeSale
        self.navigationController!.pushViewController(destVC, animated: true)
    }
    
    func didFinishFilter(controller: FilterViewController) {
        minPrice    = controller.minPrice
        maxPrice    = controller.maxPrice
        isWholeSale = controller.isWholeSale
        getData(false)
    }
}
