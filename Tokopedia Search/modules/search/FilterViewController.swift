//
//  FilterViewController.swift
//  Tokopedia Search
//
//  Created by Rafly Prayogo on 16/07/18.
//  Copyright © 2018 Tokopedia. All rights reserved.
//

import UIKit
import RangeSeekSlider

class FilterViewController: UIViewController, RangeSeekSliderDelegate {
    @IBOutlet weak var minPriceTf: UITextField!
    @IBOutlet weak var maxPriceTf: UITextField!
    @IBOutlet weak var wholeSaleSwitch: UISwitch!
    @IBOutlet weak var priceRange: RangeSeekSlider!
    
    var delegate: FilterDelegate! = nil
    
    var minPrice    = 0
    var maxPrice    = 1000000
    var isWholeSale = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        priceRange.delegate = self
        priceRange.selectedMinValue = CGFloat(minPrice)
        priceRange.selectedMaxValue = CGFloat(maxPrice)
        
        minPriceTf.text = "Rp "+minPrice.stringWithSepator
        maxPriceTf.text = "Rp "+maxPrice.stringWithSepator
        
        let btnReset = UIButton(type: .custom)
        btnReset.setTitle("Reset", for: .normal)
        btnReset.setTitleColor(UIColor(hex: "43A047"), for: .normal)
        btnReset.addTarget(self, action: #selector(self.resetFilter), for: .touchDown)
        btnReset.frame = CGRect(x: 0, y: 0, width: 50, height: 25)
        btnReset.applyNavBarConstraints(width: 50, height: 25)
        
        wholeSaleSwitch.addTarget(self, action:#selector(self.wholeSaleSwitchValueChanged(_:)), for: .valueChanged)
        
        self.navigationItem.setRightBarButton(UIBarButtonItem(customView: btnReset), animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        minPriceTf.text = "Rp "+Int(minValue).stringWithSepator
        maxPriceTf.text = "Rp "+Int(maxValue).stringWithSepator
        minPrice = Int(minValue)
        maxPrice = Int(maxValue)
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
    
    @objc func resetFilter(){
        minPriceTf.text = "Rp 0"
        maxPriceTf.text = "Rp 1.000.000"
        minPrice = 0
        maxPrice = 1000000
        priceRange.selectedMinValue = CGFloat(0)
        priceRange.selectedMaxValue = CGFloat(1000000)
        priceRange.reloadInputViews()
    }
    
    @IBAction func btnApply(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        delegate.didFinishFilter(controller: self)
    }
    
    @objc func wholeSaleSwitchValueChanged(_ sender : UISwitch!){
        if sender.isOn {
            isWholeSale =  true
        } else {
            isWholeSale =  false
        }
    }
}

protocol FilterDelegate {
    func didFinishFilter(controller: FilterViewController)
}
